/*
 * Copyright 2016-2019 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.examples.echo.ws;

import javax.websocket.server.ServerEndpoint;

import cito.server.AbstractEndpoint;
import cito.server.ws.WebSocketConfigurator;
import cito.stomp.ws.FrameDecoder;
import cito.stomp.ws.FrameEncoder;

/**
 * 
 * @author Daniel Siviter
 * @since v1.0 [14 Oct 2019]
 *
 */
@ServerEndpoint(
		value = "/websocket",
		subprotocols = { "v12.stomp", "v11.stomp", "v10.stomp" },
		encoders = { FrameEncoder.Binary.class, FrameEncoder.Text.class },
		decoders = { FrameDecoder.Binary.class, FrameDecoder.Text.class },
		configurator = WebSocketConfigurator.class)
public class Endpoint extends AbstractEndpoint {

}
